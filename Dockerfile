
# First stage: complete build environment
FROM maven:3.5.0-jdk-8-alpine AS builder

# add pom.xml and source code
ADD ./pom.xml pom.xml
ADD ./src src/

# package jar
RUN mvn clean package
## Second stage: minimal runtime environment
#From openjdk:8-jre-alpine

## copy jar from the first stage
#COPY --from=builder target/sparkjava-hello-world-1.0.war sparkjava-hello-world-1.0.war

#EXPOSE 8080

#CMD ["java", "-war", "sparkjava-hello-world-1.0.war"]
########
#FROM maven AS build
#WORKDIR /app
#COPY . .
#RUN mvn clean package

FROM tomcat:9
COPY --from=builder /target/sparkjava-hello-world-1.0.war /usr/local/tomcat/webapps 
